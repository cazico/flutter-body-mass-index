import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'icon_content.dart';
import 'reusable_widget.dart';
import 'constant.dart';
import 'calclulator_brain.dart';
import 'result_page.dart';

enum Gender { male, female }

class BMICalculator extends StatefulWidget {
  @override
  _BMICalculatorState createState() => _BMICalculatorState();
}

class _BMICalculatorState extends State<BMICalculator> {
  Gender selectedGender = Gender.male;
  int height = 180;
  int weight = 70;
  int age = 28;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGender = Gender.male;
                        });
                      },
                      child: ReusableCard(
                        colour: selectedGender == Gender.male
                            ? kActiveCardColour
                            : kInactiveCardColour,
                        cardChild: IconContent(
                          icon: FontAwesomeIcons.mars,
                          label: 'MALE',
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGender = Gender.female;
                        });
                      },
                      child: ReusableCard(
                        colour: selectedGender == Gender.female
                            ? kActiveCardColour
                            : kInactiveCardColour,
                        cardChild: IconContent(
                          icon: FontAwesomeIcons.venus,
                          label: 'FEMALE',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ReusableCard(
                colour: kActiveCardColour,
                cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'HEIGHT',
                      style: kLabelTextStyle,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: <Widget>[
                        Text(
                          height.toString(),
                          style: kNumberTextStyle,
                        ),
                        Text(
                          'cm',
                          style: kLabelTextStyle,
                        ),
                      ],
                    ),
                    Slider(
                      value: height.toDouble(),
                      min: 120.0,
                      max: 220.0,
                      onChanged: (double newValue) {
                        setState(() {
                          height = newValue.toInt();
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: ReusableCard(
                      colour: kActiveCardColour,
                      cardChild: ButtonGroup(
                        defaultValue: weight,
                        caption: 'WEIGHT',
                        onPressPlus: () {
                          setState(() {
                            weight++;
                          });
                        },
                        onPressMinus: () {
                          setState(() {
                            weight--;
                          });
                        },
                        heroTagPlus: 'weightPlus',
                        heroTagMinus: 'weightMinus',
                      ),
                    ),
                  ),
                  Expanded(
                    child: ReusableCard(
                      colour: kActiveCardColour,
                      cardChild: ButtonGroup(
                        defaultValue: age,
                        caption: 'AGE',
                        onPressPlus: () {
                          setState(() {
                            age++;
                          });
                        },
                        onPressMinus: () {
                          setState(() {
                            age--;
                          });
                        },
                        heroTagPlus: 'agePlus',
                        heroTagMinus: 'ageMinus',
                      ),
                    ),
                  ),
                ],
              ),
            ),
            new BottomButton(
              onTap: () {
                CalculatorBrain cal =
                    CalculatorBrain(height: height, weight: weight);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ResultPage(
                      resultBMI: cal.calculateBMI(),
                      resultInterpretation: cal.getInterpretation(),
                      resultText: cal.getResult(),
                    ),
                  ),
                );

//                Navigator.pushNamed(context, '/result_page',
//                    arguments: ResultPage(
//                      resultValue: cal.calculateBMI(),
//                      resultInterpretation: cal.getInterpretation(),
//                      resultBMI: cal.getResult(),
//                    ));
              },
              caption: 'CALCULATE',
            ),
          ],
        ),
      ),
    );
  }
}
