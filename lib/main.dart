import 'package:bmi_calculator/constant.dart';
import 'package:flutter/material.dart';
import 'input_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark().copyWith(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: kPrimaryColor,
        sliderTheme: SliderThemeData(
          activeTrackColor: Colors.white,
          inactiveTrackColor: Color(0xFF8D8E98),
          thumbColor: Color(0xFFEB1555),
          overlayColor: Color(0x29EB1555),
          thumbShape: RoundSliderThumbShape(
            enabledThumbRadius: 15.0,
          ),
          overlayShape: RoundSliderOverlayShape(
            overlayRadius: 30.0,
          ),
        ),
      ),
      home: BMICalculator(),
//      initialRoute: '/',
////      onGenerateRoute: (settings) {
////        final args = settings.arguments;
////        switch (settings.name) {
////          case '/':
////            return args;
////          case '/result_page':
////            return args;
////          default:
////            return null;
////        };
//      //},
//      routes: {
//        '/': (context) => BMICalculator(),
//        '/result_page': (context) => ResultPage(),
//      },
    );
  }
}