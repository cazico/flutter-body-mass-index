import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'constant.dart';

class ReusableCard extends StatelessWidget {
  ReusableCard({@required this.colour, this.cardChild});

  final Color colour;
  final Widget cardChild;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: cardChild,
      margin: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: colour,
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }
}

class BottomButton extends StatelessWidget {
  BottomButton({@required this.caption, @required this.onTap});

  final String caption;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Center(
          child: Text(
            caption,
            style: kButtonTextStyle,
          ),
        ),
        color: kBottomContainerColour,
        margin: EdgeInsets.only(top: 10.0),
        width: double.infinity,
        height: kBottomContainerHeight,
      ),
    );
  }
}

class ButtonGroup extends StatefulWidget {
  ButtonGroup(
      {@required this.caption,
      @required this.defaultValue,
      @required this.onPressPlus,
      @required this.onPressMinus,
      @required this.heroTagPlus,
      @required this.heroTagMinus});

  final String caption;
  final int defaultValue;
  final Function onPressPlus;
  final Function onPressMinus;
  final String heroTagPlus;
  final String heroTagMinus;

  @override
  _ButtonGroupState createState() => _ButtonGroupState();
}

class _ButtonGroupState extends State<ButtonGroup> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          widget.caption,
          style: kLabelTextStyle,
        ),
        Text(
          widget.defaultValue.toString(),
          style: kNumberTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FloatingActionButton(
              heroTag: widget.heroTagPlus,
              backgroundColor: Color(0xFF4C4F5),
              child: Icon(
                FontAwesomeIcons.plus,
                color: Colors.white,
              ),
              onPressed: widget.onPressPlus,
            ),
            SizedBox(
              width: 10.0,
            ),
            FloatingActionButton(
              heroTag: widget.heroTagMinus,
              backgroundColor: Color(0xFF4C4F5),
              child: Icon(
                FontAwesomeIcons.minus,
                color: Colors.white,
              ),
              onPressed: widget.onPressMinus,
            ),
          ],
        ),
      ],
    );
  }
}
